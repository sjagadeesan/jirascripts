#
# License : GPL
# Author : Sarav
# Purpose : Bulk unwatch jira issue for the given username . Script can run only only first 1000 issues from jql results
#

import sys
import getopt
import requests
import json


## Update user name password and jira base URL  for your instance of JIRA 
username = "admin"
password = "admin123"
baseurl = "https://jira.atlassian.com"
unwatchuser = "null"
purpose = "This script can only  work up to 1000 issues on single run becasue of JIRA query result limitation. If you have more than 10000 issues , please rerun the script"
def usage():
         print "Usage: %s -u unwatchuser" % sys.argv[0]
         print purpose 
         sys.exit(2)



# Argument processing . Get the base URL from the argument 
try:
   options, remainder = getopt.getopt(sys.argv[1:], 'u:') 

except getopt.GetoptError:
    usage()

for opt,args in options:
    if opt == '-u':
        unwatchuser=args

if unwatchuser == "null":
   print "You should provide a valid username"
   usage()


# Construct JIRA URL

jira_url=baseurl + "/rest/api/2/search?jql=watcher=" + unwatchuser + "&maxResults=1000"



# Open a session and authenticate user
jira = requests.Session()
jira.auth = (username , password)

# Get results from query and load the json response
try:
	s = jira.get(jira_url,verify=False)

except:
  print "ERROR while connecting to JIRA URL "
  raise

json_datas = json.loads(s.text)


# Construct URL to send Delete data


for issue in json_datas['issues']:
   print issue['self']
   watcher_url= issue['self'] + "/watchers?username=" + unwatchuser
   p = jira.delete(watcher_url,verify=False)
   print "Removed watcher " + unwatchuser 


print "Completed"
print purpose

