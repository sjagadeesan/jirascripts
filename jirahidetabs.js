// License : GPL
// Author : Sarav 
// Puprose : Hide a Tab from issue tab panel JIRA . It's useful if you would like to hide some irrelevant tabs for specific projects 
<script type="text/javascript">
    function hideTab() {
      //get issue key from meta 
      var metas = AJS.Meta.get("issue-key");
      var prid = metas.split("-");
      if(prid.length>0) {      
                  console.log("Hiding a tab for this project");
		  if(prid [0] == "PRJ"){  
				     //I'm hding tet cases panel from Zephyr plugin but this can replaced with other panel use datid of the panel 
                      AJS.$('#testcase_panel').hide();
                                   
                }
            }
         }
         //Make it when the page loaded 
        AJS.$(document).ready(function(){
            hideTab();
            //Make it when issue tab loaded for example when user click Subversion  
             JIRA.ViewIssueTabs.onTabReady(function() {
             	hideTab();
          });
});
</script>