# README #

Collection of simple scripts for JIRA administration purposes 

### What is this repository for? ###

* It consist of various simple groovy,javascript,python scripts .
* DeleteUserComments.groovy - It will delete a comment from specific user on a given issue. This is useful if you will need to delete multiple comments by a particular users 
* jirahidetabs.js - Hide tab in  jira issue panel
* unwatch.py - Script to bulk unwatch issues for given user.

### How do I get set up? ###

You will need to installed script runner plugin to use the groovy scripts for javascripts you will need to bundle it with your plugins or copy them in field configuration
For python scripts, please install requests module


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact